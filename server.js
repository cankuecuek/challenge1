const Hapi = require('@hapi/hapi');
const Joi = require('joi');
const axios = require('axios');
const moment = require('moment-timezone');

// In-memory data store
let orders = [];
let orderIDcount = 1;

// Order schema
const schema = Joi.object({
    assetType: Joi.string().valid('CRYPTO').required(),
    symbol: Joi.string().required(),
    amount: Joi.number().strict().greater(0).required(),
    side: Joi.string().valid('BUY', 'SELL').required(),
    orderType: Joi.string().valid('MARKET').required()
});

// Function to get price from Binance API
const getPrice = async (symbol) => {
    const url = "https://api.binance.com/api/v3/ticker/price";
    const requestSymbol = symbol + "USDT";
    try {
        const response = await axios.get(url, { params: { symbol: requestSymbol } });
        const data = response.data;
        return parseFloat(data.price);
    } catch (error) {
        console.error('Error fetching price:', error);
        return null;
    }
};

const init = async () => {
    const server = Hapi.server({
        port: 3000,
        host: '0.0.0.0' // Listen on all interfaces
    });

    // POST /orders route
    server.route({
        method: 'POST',
        path: '/orders',
        handler: async (request, h) => {
            const data = request.payload;
            try {
                console.log('Received payload:', data); // Log received payload

                // Validate data against the schema
                await schema.validateAsync(data, { abortEarly: false });

                const quote = await getPrice(data.symbol);
                if (!quote) {
                    return h.response({ error: 'Failed to fetch price' }).code(406);
                }

                const total = data.amount * quote;
                const time = moment().tz('UTC').format();

                data.placedAt = time;
                data.total = total;
                data.quote = quote;

                orderIDcount += 1;
                data.orderID = String(orderIDcount);

                orders.push(data);

                return h.response(data).code(200);
            } catch (error) {
                console.error('Validation or other error:', error);
                return h.response({ error: error.isJoi ? error.details[0].message : error.message }).code(406);
            }
        }
    });

    // GET /orders route
    server.route({
        method: 'GET',
        path: '/orders',
        handler: (request, h) => {
            return h.response(orders).code(200);
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();
