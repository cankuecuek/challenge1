# Crypto Trading API
## Description
This Crypto Trading API is a web service that simulates a cryptocurrency trading platform. It allows users to create and retrieve orders with live cryptocurrency prices from Binance. It is built using Node.js with the Hapi framework and can be containerized using Docker.

## Installation
Please follow the steps below for successful use. (MacOS)
## Requirements
[Node.js](https://nodejs.org/en) v20.13.1

[Docker](https://docs.docker.com/get-docker/)

### Steps
1. Clone the repository:

```bash
git clone https://gitlab.com/cankuecuek/challenge1.git
cd challenge1
```

2. Install the dependencies:
```bash
npm install
```

## Usage
### Running the service with Docker

1. Build and run the Docker Container (there is an existing dockerfile):

```bash
 docker-compose up --build 
```

2. The servier will be running on http://0.0.0.0:3000


## API Endpoints

### Create Order

**URL:** `POST /orders`

**Request Body:**
```json
{
  "symbol": "ETH",
  "assetType": "CRYPTO",
  "amount": 1.5,
  "orderType": "MARKET",
  "side": "BUY"
}
```

**Response:**
```json
{
  "orderID": "123456789",
  "assetType": "CRYPTO",
  "symbol": "ETH",
  "amount": 1.5,
  "side": "BUY",
  "orderType": "MARKET",
  "quote": 2500.00,
  "total": 3750.00,
  "placedAt": "2023-01-01T12:00:00Z"
}
```

### Retrieve Orders

**URL:** `GET /orders`

**Response:**
```json
[
  {
    "orderID": "123456789",
    "assetType": "CRYPTO",
    "symbol": "ETH",
    "amount": 1.5,
    "side": "BUY",
    "orderType": "MARKET",
    "quote": 2500.00,
    "total": 3750.00,
    "placedAt": "2023-01-01T12:00:00Z"
  }
]
```

## Testing

1. You can use Postman or any API testing tool to make requests to the API endpoints.

2. Additionally we added an test.hurl file to run automated tests on the API, please us following command:
```bash
hurl test.hurl
```

## Support

Please feel free to contact us under [cryptoAPI@gmail.com](mailto:cryptoAPI@gmail.com) if you need any support. 

## Roadmap

We plan to add a GUI and store our order data in a MongoDB database. Please watch this space for updates.

## Authors

Luca F., Simon S., Can K.

## License

MIT License

Copyright© [2024] [CryptoAPI]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



